/*	PURPOSE:
	This file manages the control UI */

(function() {
	ui = {
		init: function () {
		// Set the background
			if (scope.config.bgSelected === undefined) {
				scope.config.bgSelected = defaultConfig.bgImages[0].src;
			};
			ui.setBG(scope.config.bgSelected);
			console.info('[2] UI INIT Complete');
		},
		famSearch: function (input) {
			var inputSrc = document.querySelector('#searchFamInput');
			if (isNaN(input)) { inputSrc.type = "text" }
			else {inputSrc.type = 'password'}
			if (input == scope.config.security.adminCode.value) {
				ui.adminMenuShow();
				return false;
			};
			scope.famMatch = [];
			var fams = scope.data.families;
			if (fams !== undefined) {
				for (var i = 0; i < fams.length; i++) {
					var test = fams[i].name.toLowerCase();
					if (input.length > 0 && fams[i].active && input.toLowerCase() == test.substring(0, input.length)) {
						fams[i].index = i;
						scope.famMatch.push(fams[i]);
					}
				};
			}
			ui.dispReset(ui.disp.header);
			if (inputSrc.value.length > 0 && scope.config.security.addFamilies.value) {
				scope.famMatch.push({
					active: true,
					name: "Not Listed",
					adult1: "Add Family",
					kids: true,
					members: []
				});
			}
			if (scope.famMatch.length > 0) {
				ui.disp.header.famChoose = true;
				ui.disp.mid.goBack = true;
			}
			else {
				ui.disp.header.welcome = true;
				ui.disp.mid.goBack = false;
			}
		},
		famSelect: function (el) {
			scope.focusFam = el;
			if (el.name == 'Not Listed') {
				ui.prepFam();
				return false;
			};
			scope.focusFam.name = el.name;
			ui.dispReset();
			ui.disp.header.checkSelect = true;
			ui.disp.mid.goBack = true;
			ui.disp.mid.famEdit = true;
			ui.disp.block.checkSelect = true;
			setTimeout(function() {
				scope.$apply();
			}, 10);
			setTimeout(function() {
				var fam = el.members;
				var box = document.querySelector('#checkSelectBlock');
				box.className = 'checkPrintables';
				var memDiv = document.createElement('div');
				memDiv.id = 'chechPrintMems';
				memDiv.className = 'chechPrintMems';
				// memDiv.innerHTML = '<span>Print Stickers For:</span><hr />';
				box.appendChild(memDiv);
				for (var i = fam.length - 1; i >= 0; i--) {
					var mem = fam[i];
					if (mem.grade != 'Adult') {	
						var input = document.createElement('input');
						input.type = 'checkbox';
						input.name = ui.capFirst(mem.first);
						input.className = 'printInput';
						input.dataset.name = ui.capFirst(mem.first);
						input.dataset.lastname = ui.capFirst(mem.last);
						input.dataset.dob = mem.dob;
						input.dataset.gender = mem.gender;
						input.dataset.allergies = mem.allergies;
						input.id = 'checkin_'+mem.first;
						input.checked = true;
						if (scope.focusFam.name == scope.config.ui["staffLabel"].value) {
							input.checked = false;	
						}
						var label = document.createElement('label');
						label.appendChild(input);
						label.htmlFor = input.id;
						label.appendChild(document.createTextNode(' '+ui.capFirst(mem.first)));
						memDiv.appendChild(label);
					};
				};
				if (fam.length > 0) {				
					var printDiv = document.createElement('div');
					printDiv.className = 'printDiv';
					var printButton = document.createElement('button');
					printButton.innerHTML = 'Print';
					printButton.setAttribute("onclick", 'ui.printLabels()');
					printDiv.appendChild(printButton);
					box.appendChild(printDiv);
				};
			}, 20);
		},
		prepFam: function() {
			if (scope.focusFam.name == 'Not Listed') {
				scope.focusFam = {name: '', edit: 'addFamily', adult1: '', adult2: '', kids: false, active: false, members: []};
			};
			ui.dispReset();
			ui.disp.header.newCheckin = true;
			// ui.disp.mid.goBack = true;
			ui.disp.block.editFam = true;
			ui.disp.block.keyB = true;
			setTimeout(function() { scope.$apply(); }, 5);
			setTimeout(function() {
				if (document.querySelector('#newFamName') != null) {
					document.querySelector('#newFamName').focus();
				};
			}, 10);
		},
		editFam: function(context) {
			if (context === undefined) { context = scope.focusFam.edit; };
			var nameInput = document.querySelector('#newName');
			var allergyInput = document.querySelector('#newAllergies');
			var dobInput = document.querySelector('#newDob');
			var genderInput = document.querySelector('#newGender');
			switch (context) {
				case 'addFamily':
					var nameInput = document.querySelector('#newFamName');
					if (nameInput === undefined || ui.isValidInput(nameInput.value, 20) == false) {return false;}
				// Feed the new template into the data scope
					scope.focusFam.name = nameInput.value;
					scope.focusFam.edit = "";
				break;
				case 'addAdult':
					if (scope.focusFam.adult1 == '') { scope.focusFam.adult1 = nameInput.value; }
					else if (scope.focusFam.adult2 == '') { scope.focusFam.adult2 = nameInput.value; };
					var newMem = {
						first: nameInput.value,
						last: scope.focusFam.name,
						// dob: dobInput.value,
						gender: genderInput.value,
						allergies: allergyInput.value,
						grade: 'Adult'
					}
					scope.focusFam.members.push(newMem);
					nameInput.value = '';
					scope.focusFam.edit = "";
				break;
				case 'addChild':
					var newMem = {
						first: nameInput.value,
						last: scope.focusFam.name,
						dob: dobInput.value,
						gender: genderInput.value,
						allergies: allergyInput.value,
						grade: 'Child'
					}
					scope.focusFam.members.push(newMem);
					nameInput.value = '';
					scope.focusFam.edit = "";
				break;
				case 'removeFamily':
					var uSure = confirm('Are you sure you want to remove the ['+scope.focusFam.name+'] family?');
					if (uSure && scope.focusFam.index !== undefined && !isNaN(scope.focusFam.index)) {
						scope.data.families.splice(scope.focusFam.index, 1);
						console.log('removed index '+scope.focusFam.index);
						scope.focusFam = {};
						ui.overwriteLS('data');
						scope.goBack();
					}
				break;
				case 'removeMember':
					var uSure = confirm('Are you sure you want to remove ['+scope.focusMem.first+'] from this family?');
					if (uSure) {
						for (var i = 0; i < scope.focusFam.members.length; i++) {
							var unit = scope.focusFam.members[i];
							if (unit.$$hashKey !== undefined && scope.focusMem.$$hashKey !== undefined && scope.focusMem.$$hashKey == unit.$$hashKey) {
								scope.focusFam.members.splice(i, 1);
								scope.focusFam.edit = '';
								ui.overwriteLS('data');
							}
						};
					}
				break;
				case 'update':
					var key = scope.focusMem.$$hashKey;
					if (key !== undefined && scope.focusFam.members.length > 0) {
						for (var i = 0; i < scope.focusFam.members.length; i++) {
							if (scope.focusFam.members[i].$$hashKey == key) {
								scope.focusFam.members[i].first = document.querySelector('#newName').value;
								scope.focusFam.members[i].allergies = document.querySelector('#newAllergies').value;
								scope.focusFam.members[i].dob = document.querySelector('#newDob').value;
								scope.focusFam.members[i].gender = document.querySelector('#newGender').value;
							}
						};
					};
					scope.focusMem = {};
					scope.focusFam.edit = '';
					ui.overwriteLS('data');
				break;
				default:
					// EDIT MEMBER
					var mem = context.mem;
					scope.focusMem = mem;
					scope.focusFam.edit = 'edit';
					setTimeout(function() {
						scope.$apply();
					}, 5);
					setTimeout(function() {
						var nameInput = document.querySelector('#newName');
						var allergyInput = document.querySelector('#newAllergies');
						allergyInput.value = mem.allergies;
						nameInput.value = mem.first;
					}, 10);
				break;
				case 'done':
					scope.focusFam.active = true;
					scope.focusFam.edit = '';
					if (scope.focusFam.index !== undefined) {
					// UPDATE EXISITING FAMILY
						if (scope.data.families[scope.focusFam.index] !== undefined) {
							scope.data.families[scope.focusFam.index] = scope.focusFam;
						}
					}
					else {
					// ADD NEW FAMILY
						scope.data.families.push(scope.focusFam);
					}
					ui.overwriteLS('data');
					ui.famSelect(scope.focusFam);
				break;
			}
		},
		printLabels: function(registered) {
			var target = document.querySelector('#printMe');
			target.innerHTML= '';
			var code = ui.codeGen();
			var labelData = [];
			if (scope.focusFam.name != scope.config.ui.staffFamily.value) {
				// Make the Parent pickup card
				var input = document.createElement('input');
					input.dataset.name = "Pickup Card";
					input.dataset.dob = "";
					input.checked = true;
					input.className = 'printInput hidden';
				labelData.push(input);
				if (document.querySelector('#checkSelectBlock') !== null) {
					document.querySelector('#checkSelectBlock').appendChild(input);
				};
			}
			if (registered == 0) {
				// GENERIC VISITOR TEMPLATE
				var count = parseInt(document.querySelector('#visitPrintCount').innerHTML);
				if (isNaN(count)) {count = 1;};
				for (var i = 0; i < count; i++) {
					var input = document.createElement('input');
						input.dataset.name = "&nbsp;";
						input.dataset.dob = "";
						input.dataset.allergies = "____________________";
						input.checked = true;
					labelData.push(input);
					ui.logCheckin();
				};
			}
			else {
				// SELECTED REGISTERED FAMILY MEMBERES
				var labelData = document.querySelectorAll('.printInput');
			}
			for (var i = 0; i < labelData.length; i++) {
				if (labelData[i].checked) {
					var name = labelData[i].dataset.name;
					var dob = labelData[i].dataset.dob;
					var allergies = labelData[i].dataset.allergies;
					if (allergies == '') { allergies = 'None'; }
					var labelMsg = scope.config.ui.labelMsg.value;
					if (labelMsg == '') { labelMsg = scope.config.ui.ministryName.value; };
					var label = document.createElement('div');
					label.className = 'printLabel';
					target.appendChild(label);
					var span1 = document.createElement('span');
						span1.className = 'name';
						span1.innerHTML = ui.capFirst(name);
					label.appendChild(span1);
					var span2 = document.createElement('span');
						span2.className = 'labelMsg';
						span2.innerHTML = labelMsg;
						if (name != "Pickup Card" && registered != 0) {
							span2.innerHTML = scope.config.encouragements[Math.floor(Math.random()*scope.config.encouragements.length)];
						}
						if (name != "Pickup Card" && registered == 0) {
							span2.innerHTML = 'Name';
						};
						// Determine how far away the birthday is from today
						var bDayAway = ui.daysToDob(dob);
						if (bDayAway > -1 && bDayAway < 7) {
							span2.innerHTML = 'It\'s My Birthday Week!';
						}
					label.appendChild(span2);
				// Check if STAFF or NOT
					var span3 = document.createElement('span');
						span3.className = 'allergies';
						if (name != "Pickup Card") {
							span3.innerHTML = 'Allergies: '+allergies;
						}
						else {
							span3.innerHTML = "Date: "+new Date().toLocaleDateString();
						}
					label.appendChild(span3);
					var span4 = document.createElement('span');
						span4.className = 'code';
						span4.innerHTML = '<i>'+scope.config.ui.labelCode.value+': </i><b>'+code+'</b>';
					label.appendChild(span4);
					if (scope.focusFam.name == scope.config.ui.staffFamily.value) {
						// span3.innerHTML = scope.config.encouragements[Math.floor(Math.random()*scope.config.encouragements.length)];
						span3.innerHTML = scope.config.ui.ministryName.value;
						span2.innerHTML = scope.config.ui.staffLabel.value;
						span4.innerHTML = "Date: "+new Date().toLocaleDateString();
					}
					if (name != "Pickup Card" && registered != 0) {
						ui.logCheckin(labelData[i].dataset);
					}
				}
			};
			if (target.innerHTML != '') {
				window.print();
				target.innerHTML = '';
				ui.reset();
			};
		},
		printReport: function(report) {
			if (report === undefined) { return false; };
			var target = document.querySelector('#printMe');
			target.innerHTML= '';
			switch (report) {
				case 'daily':
					var label = document.createElement('div');
					label.className = 'printLabel';
						var title = document.createElement('span');
							title.innerHTML = 'Check-in activity for '+new Date().toDateString();
							title.className = 'reportTitle';
							label.appendChild(title);
						var year = new Date().getFullYear();
						var month = new Date().getMonth() + 1;
						var day = new Date().getDate();
						var data = scope.reportData[year][month][day];
						if (data !== undefined) {
						// Total
							var group = document.createElement('div');
							group.className = 'reportGroup';
							var unit = document.createElement('span');
								unit.innerHTML = 'Check-ins: ';
								unit.className = 'reportUnit';
							label.appendChild(unit);
							var value = document.createElement('span');
								value.className = 'reportValue';
								value.innerHTML = 'Total: '+data.total+' &nbsp; ';
							group.appendChild(value);
							var value = document.createElement('span');
								value.className = 'reportValue';
								value.innerHTML = 'Visitors: '+data.visitors;
							group.appendChild(value);
							label.appendChild(group);
						// Genders
							var group = document.createElement('div');
							group.className = 'reportGroup';
							var unit = document.createElement('span');
								unit.className = 'reportUnit';
								unit.innerHTML = 'Genders: ';
							label.appendChild(unit);
							var value = document.createElement('span');
								value.className = 'reportValue';
								value.innerHTML = 'Boys: '+data.genders.male+' &nbsp; ';
							group.appendChild(value);
							var value = document.createElement('span');
								value.className = 'reportValue';
								value.innerHTML = 'Girls: '+data.genders.female+'';
							group.appendChild(value);
							label.appendChild(group);
						// Ages
							var group = document.createElement('div');
							group.className = 'reportGroup';
							var unit = document.createElement('span');
								unit.className = 'reportUnit';
								unit.innerHTML = 'Ages: ';
							label.appendChild(unit);
							for (year in data.age ) {							
								var value = document.createElement('span');
									value.className = 'reportValue';
									value.innerHTML = ''+year+'yr: '+data.age[year]+' &nbsp; ';
								group.appendChild(value);
								label.appendChild(group);
							}
						}
						else {
							var noData = document.createElement('span');
							noData.className = 'noData';
							noData.innerHTML = 'Sorry<br/>Nothing to Report';
							label.appendChild(noData);
						}
						var footer = document.createElement('span');
							footer.innerHTML = scope.config.ui.ministryName.value;
							footer.className = 'reportFooter';
							label.appendChild(footer);
					target.appendChild(label);
				break;
			}
			if (target.innerHTML != '') {
				window.print();
				target.innerHTML = '';
			};
		},
		codeGen: function() {
			var length = 4;
			var chars = '12345789BEGHKLMNOPQRTVWXYZ';
			var result = '';
			for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
			return result;
		},
		daysToDob: function(dob, now) {
			if (dob === undefined || dob == null || dob == '') { return -1;};
			if (now === undefined || now == null || now == '') {
				// Need code to compensate for birthdays in the first few days of Jan, when Last sunday in Dec is > 12/25
				var now = (new Date().getMonth()+1)+'/'+new Date().getDate()+'/'+new Date().getFullYear()
			}
			var comp = (new Date(dob).getMonth()+1)+'/'+new Date(dob).getDate()+'/'+new Date().getFullYear()
			var daysDiff = (new Date(comp) - new Date(now)) / 1000 / 24 / 60 / 60;
			return daysDiff +1;
		},
		newCheckin: function() {
			ui.dispReset();
			ui.disp.header.newCheckin = true;
			ui.disp.mid.goBack = true;
			ui.disp.block.newCheckin = true;
			setTimeout(function() {
				var box = document.querySelector('#newCheckinBlock');				
				var printDiv = document.createElement('div');
					printDiv.className = 'printDiv';
				var printButton = document.createElement('button');
					printButton.innerHTML = 'Print';
					printButton.setAttribute("onclick", 'ui.printLabels(0)');
					printDiv.appendChild(printButton);
				box.appendChild(printDiv);
			}, 10);
		},
		adminMenuShow: function() {
			ui.dispReset();
			ui.disp.header.admin = true;
			ui.disp.mid.goBack = true;
			ui.disp.block.admin = true;
		},
		adminMenuContent: function(group) {
			if (group === undefined) { return false; }
			switch (group) {
				case 'text':
					var headText = 'Interface Text Settings';
					var content = document.createElement('div');
					content.className = 'settingsText'
					for (key in scope.config.ui ) {
						var unit = scope.config.ui[key];
						var span = document.createElement('span');
						span.innerHTML = unit.title;
						content.appendChild(span);
						var input = document.createElement('input');
						input.type = unit.type;
						if (unit.type == 'checkbox' && unit.value == 1) { input.checked = true; };
						input.value = unit.value;
						input.setAttribute('onchange', 'ui.applySetting(scope.config.ui["'+key+'"], this.value)');
						content.appendChild(input);
					}
				break;
				case 'data':
					var headText = 'Data Management';
					var content = document.createElement('div');
					var subDiv1 = document.createElement('div');
						subDiv1.className = 'flexNoWrap';
						var button1 = document.createElement('button');
							button1.innerHTML = "Export Settings";
							button1.className = "blockMed good";
							button1.setAttribute('onclick', '');
						subDiv1.appendChild(button1);
						var button2 = document.createElement('button');
							button2.innerHTML = "Export Data";
							button2.className = "blockMed good";
							button2.setAttribute('onclick', '');
						subDiv1.appendChild(button2);
					content.appendChild(subDiv1);
					var subDiv2 = document.createElement('div');
						subDiv2.className = 'flexNoWrap';
						var button3 = document.createElement('button');
							button3.innerHTML = "Import Settings";
							button3.className = "blockMed";
							button3.setAttribute('onclick', '');
						subDiv2.appendChild(button3);
						var button4 = document.createElement('button');
							button4.innerHTML = "Import Data";
							button4.className = "blockMed";
							button4.setAttribute('onclick', '');
						subDiv2.appendChild(button4);
					content.appendChild(subDiv2);
					var subDiv3 = document.createElement('div');
						subDiv3.className = 'flexNoWrap';
						var button5 = document.createElement('button');
							button5.innerHTML = "Clear Family Data";
							button5.className = "blockMed warn";
							// CHANGE THIS TO A CONFIRM FOR SAFETY EVENTUALLY
							button5.setAttribute('onclick', 'ui.resetData()');
						subDiv3.appendChild(button5);
						var button6 = document.createElement('button');
							button6.innerHTML = "Default all Settings";
							button6.className = "blockMed warn";
							// CHANGE THIS TO A CONFIRM FOR SAFETY EVENTUALLY
							button6.setAttribute('onclick', 'ui.resetSettings()');
						subDiv3.appendChild(button6);
					content.appendChild(subDiv3);
					var subDiv4 = document.createElement('div');
						subDiv4.className = 'flexNoWrap';
						var button7 = document.createElement('button');
							button7.innerHTML = "Clear Report Data";
							button7.className = "blockMed warn";
							// CHANGE THIS TO A CONFIRM FOR SAFETY EVENTUALLY
							button7.setAttribute('onclick', 'ui.resetData()');
						subDiv4.appendChild(button7);
					content.appendChild(subDiv4);
				break;
				case 'apperance':
					var headText = 'Appearance Settings';
					var content = document.createElement('div');
					var bgDiv = document.createElement('div');
						bgDiv.className = 'thumbGallery';
						for (var i = 0; i < defaultConfig.bgImages.length; i++) {
							var imgData = defaultConfig.bgImages[i];
							var subDiv = document.createElement('div');
								var deg = Math.random() * 1.5;
								var plusOrMinus = Math.random() < 0.5 ? -1 : 1;
								subDiv.style.transform = 'rotate('+(deg * plusOrMinus)+'deg)'
								var img = document.createElement('img');
									img.src = imgData.src;
								subDiv.appendChild(img);
								var span = document.createElement('span');
									span.innerHTML = imgData.title;
								subDiv.appendChild(span);
								subDiv.setAttribute("onclick", 'ui.setBG("'+imgData.src+'")');
							bgDiv.appendChild(subDiv);
						};
					content.appendChild(bgDiv);
				break;
				case 'reports':
					var headText = 'Activity Reports';
					var content = document.createElement('div');
						var reportDiv = document.createElement('div');
						reportDiv.className = 'reportDiv';
						for (key in scope.config.reports ) {
							var unit = scope.config.reports[key];
							if (unit.type == 'button') {
								var button = document.createElement('button');
								button.setAttribute('onclick', 'ui.printReport("'+unit.value+'")');
								button.innerHTML = 'Print';
								reportDiv.appendChild(button);							
							};
							var span = document.createElement('span');
							span.innerHTML = unit.title;
							reportDiv.appendChild(span);
						}
					content.appendChild(reportDiv);
				break;
				case 'security':
					var headText = 'Security Settings';
					var content = document.createElement('div');
					content.className = 'settingsText'
					for (key in scope.config.security ) {
						var unit = scope.config.security[key];
						var span = document.createElement('span');
						span.innerHTML = unit.title;
						content.appendChild(span);
						var input = document.createElement('input');
						input.type = unit.type;
						input.value = unit.value;
						if (unit.type == 'checkbox') { 
							if (unit.value === true) { input.checked = true; };							
							input.setAttribute('onchange', 'ui.applySetting(scope.config.security["'+key+'"], this.checked)');
						}
						else {
							input.setAttribute('onchange', 'ui.applySetting(scope.config.security["'+key+'"], this.value)');
						}
						content.appendChild(input);
					}
				break;
			}
			if (headText !== undefined && content !== undefined) {
				var box = document.querySelector('#adminRight');
				box.innerHTML = '';
				var title = document.createElement('span');
				title.className = 'blockHeader';
				title.innerHTML = headText;
				box.appendChild(title);
				box.appendChild(content);
			};
		},
		applySetting: function(key, value) {
		// Update the Scope
			console.log(key.checked);
			key.value = value;
			scope.$apply();
		// Update browser localStorage
			ui.overwriteLS('config');
		},
		overwriteLS: function(group) {
		// PURPOSE: Overwrites all or part of the keys in local storage
			if (group === undefined) { return false; };
			switch (group) {
				case 'data':
				// Check for duplicates in the data
					localStorage.appData = JSON.stringify(scope.data);
					// console.log('overwrite-d data')
				break;
				case 'config':
					localStorage.appConfig = JSON.stringify(scope.config);
					// console.log('overwrite-d config')
				break;
				case 'checkins':
					localStorage.reportData = JSON.stringify(scope.reportData);
					// console.log('overwrite-d checking')
				break;
				default:
					// statements_def
				break;
			}
		},
		changeVisitors: function(num) {
			if (num === undefined) { return false;};
			scope.visitKids += num;
			if (scope.visitKids <= 0 ) {scope.visitKids = 1};
		},
		reset: function() {
			ui.dispReset();
			scope.focusFam = {name: 'Family Actions', adults: 0, kids: 0, active: false, members: []};
			ui.disp.header.welcome = true;
			ui.disp.mid.famSearch = true;
			ui.disp.block.keyB = true;
			scope.visitKids = 1;
			setTimeout(function() {
				scope.$apply();
					document.querySelector('#searchFamInput').value = '';
			}, 10);
			ui.keyBFocus();
		},
		resetData: function() {
			var c = confirm("You really want to erase all your data? - Last chance to turn back"); 
			if (c) {
				scope.data = defaultData;
				ui.overwriteLS('data');
			}

		},
		resetSettings: function() {
			var c = confirm("You really want to return all your custom settings to the defaults?"); 
			if (c) {
				localStorage.appConfig = JSON.stringify(defaultConfig);
				scope.config = JSON.parse(localStorage.appConfig);
				scope.$apply();
				ui.overwriteLS('config');
			}

		},
		isValidInput: function(input, lengthLimit) {
		// PURPOSE: Checks to see if the supplied input string is safe and useful
			var valid = true;
			if (lengthLimit == undefined) {lengthLimit = 300; }
			if (input.length == 0 || input.length > lengthLimit) {valid = false;}
			return valid;
		},
		dispReset: function (scope) {
		// HIDES ALL UI ELEMENTS UNDER THE SCOPE
			if (scope === undefined) {
				ui.dispReset(ui.disp.header);
				ui.dispReset(ui.disp.mid);
				ui.dispReset(ui.disp.block);
			}
			else {
				for (var key in scope) {
					scope[key] = false;
				} 
			}
		},
		capFirst: function(input) {
			return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1) : '';
		},
		opposite: function(key) {
		// Purpose: returns the opposite value of the the supplied 'key'
			if (key == true) { return false; } else {	return true; }
			if (key == 'On') { return 'On'; } else {	return 'Off'; }
			if (key == 'Pass') { return 'Pass'; } else {	return 'Fail'; }
			if (key == 'Good') { return 'Good'; } else {	return 'Bad'; }
		},
		setBG: function(src) {
			var body = document.getElementsByTagName('body')[0];
			body.style.backgroundImage = 'url('+src+')';
			scope.config.bgSelected = src;
			ui.overwriteLS('config');
		},
		logCheckin: function(unit) {
		// Make sure the proper date structure / data structure is in place
			if (scope.reportData === undefined) { scope.reportData = {}; };
			var year = new Date().getFullYear();
			if (scope.reportData[year] === undefined) { scope.reportData[year] = {}; };
			var month = new Date().getMonth() + 1;
			if (scope.reportData[year][month] === undefined) { scope.reportData[year][month] = {}; };
			var day = new Date().getDate()
			if (scope.reportData[year][month][day] === undefined) { 
				scope.reportData[year][month][day] = {
					total: 0,
					visitors: 0,
					age: {},
					genders: {male: 0, female: 0}
				}; 
			};
			var base = scope.reportData[year][month][day];
		// Always increment total count
			base.total++; 
			if (unit === undefined) {
			// Increment Visitor count
				base.visitors++;
			}
			else {
			// Check Genders
				if (unit.gender !== undefined && unit.gender == 'M') { base.genders.male++; }
				if (unit.gender !== undefined && unit.gender == 'F') { base.genders.female++; }
			// Check Age
				if (unit.dob !== undefined && unit.dob != '') {
					var yearsOld = new Date().getFullYear() - new Date(unit.dob).getFullYear();
					if (base.age[yearsOld] === undefined) { base.age[yearsOld] = 1 }
						else { base.age[yearsOld]++; }
				};
			}
			// console.log(base)
			ui.overwriteLS('checkins');
		},
		keyBFocus: function() {
			setTimeout(function() {
					document.querySelector('#searchFamInput').focus()
			}, 10);
		},
		disp: {
			header: {
				welcome: true,
				famChoose: false,
				checkSelect: false,
				newCheckin: false,
				admin: false,
			},
			mid: {
				famSearch: true,
				goBack: false,
				famEdit: false,
				editFam: false,
			},
			block: {
				keyB: true,
				childSelect: false,
				newCheckin: false,
				admin: false,
			}
		},
	};

window.onload = function(){ ui.init() };

//Finished defining the smSampler Object
})();


