/*	PURPOSE:
	This file manages the control UI */

(function() {
	ng = {
		init: function () {
			ng.checkLS();
			app = angular.module("regMod", ['onScreenKeyboard']);	
			app.controller('regCon', ng.con = function ($scope) {
			// DATA
			// SCOPE MAP TO JS
				scope = $scope;
				$scope.data = JSON.parse(localStorage.appData);
				$scope.config = JSON.parse(localStorage.appConfig);
				$scope.reportData = JSON.parse(localStorage.reportData);
				$scope.uiDisp =  ui.disp;
				$scope.famMatch =  [];
				$scope.visitKids = 1;
				$scope.focusFam = {name: 'Family Actions', adults: 0, kids: 0, active: false, members: []};
				$scope.focusMem = {};
				$scope.bgImages = defaultConfig.bgImages;
			// FUNCTIONS
				$scope.famSearch =  ui.famSearch;
				$scope.famSelect =  ui.famSelect;
				$scope.goBack =  ui.reset;
				$scope.newCheckin = ui.newCheckin;
				$scope.focusSearch = ui.keyBFocus;
				$scope.uiReset = ui.reset;
				$scope.changeVisitors = ui.changeVisitors;
				$scope.adminMenu = ui.adminMenuContent;
				$scope.adminChange = ui.applySetting;
				$scope.prepFam = ui.prepFam;
				$scope.editFam = ui.editFam;
			});
			ng.regModules();
			console.info('[1] NG INIT Complete');
		},
		regModules: function() {
			angular.module('myAngularApp', ['onScreenKeyboard']);
			app.filter('capFirst', function() {
				return function(input) {
					return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1) : '';
				}
			});
		},
		checkLS: function() {
		// PURPOSE: Checks to ensure that local storage is provisioned
			if (localStorage.appData === undefined) {
				localStorage.appData = JSON.stringify(defaultData);
			};
			if (localStorage.appConfig === undefined) {
				localStorage.appConfig = JSON.stringify(defaultConfig);
			};
			if (localStorage.reportData === undefined) {
				localStorage.reportData = JSON.stringify({});
			};
		},
		syncStorage: function() {
			if (typeof(Storage) !== "undefined") {

			}
		},
	};

ng.init();

//Finished defining the smSampler Object
})();