defaultConfig = {
	dataReset: false,
	ui: {
		welcomeMsg: {type: 'text', title: "Welcome Message", value: "Welcome to"},
		ministryName: {type: 'text', title: "Ministry Name", value: "Horizon Kids"},
		appName: {type: 'text', title: "App Name", value: "Check-In Station"},
		newRegButton: {type: 'text', title: "Visitor Button", value: "Just Visiting Today"},
		newCheckinHeadText: {type: 'text', title: "Visitor Header", value: "Awesome! <br />We're glad you are here today."},
		newCheckinBlockText: {type: 'text', title: "Visitor Question ", value: "How many children are you checking in?"},
		famCheckinHeadText: {type: 'text', title: "Member 	Header", value: "Welcome back to "},
		famCheckinBlockText: {type: 'text', title: "Member Question", value: "Who would you like to check in?"},
		famEditButton: {type: 'text', title: "Edit Family Button", value: "Edit Family"},
		backButton: {type: 'text', title: "Back Button", value: "Go Back"},
		labelMsg: {type: 'text', title: "Label Message", value: ""},
		labelCode: {type: 'text', title: "Pickup Code Title", value: "Safety Code"},
		staffFamily: {type: 'text', title: "Staff Family Name", value: "Helpers"},
		staffLabel: {type: 'text', title: "Staff Family Label", value: "Helper"},
	},
	security: {
		addFamilies: {type: 'checkbox', title: "Allow Add Families", value: true},
		adminCode: {type: 'number', title: "Admin Password", value: "0000"},
		editFamilies: {type: 'checkbox', title: "Allow Edit Families", value: true},
	},
	reports: {
		dailyReport: {type: 'button', title: "Today's Checkin Report", value: 'daily'}
	},
	encouragements: [
		'Jesus Loves Me','Forgiven &amp; Free','Light of the World','I am Loved',
		'Free in Christ','God\'s Masterpiece','Made with a Purpose','Created for Good Works',
		'Child of Light','Loved by God','Glorious in Christ','Cherished by God','Sharing God\'s Love',
		'Jesus Died for Me','Adopted by God','Living Life Abundantly','Growing in Grace','Truth and Love',
		'Called to Love','New in Christ','Blessed by God','Childlike Faith','City on a Hill','Faith Like a Child',
		'Blessed','Child of the King','Child of God','Friend of God','Citizen of Heaven','Alive with Christ',
		'Grateful for God','Strong in Christ','Growing in Grace','Victorious','Safe with God','Sealed with Promise'
	],
	bgImages: [
		{src: 'css/images/fur.jpg', title: 'Color Fur'},
		{src: 'css/images/gradient.jpg', title: 'Gradient'},
		{src: 'css/images/circlePaint.jpg', title: 'Paint Circles'},
		{src: 'css/images/blurrySphere.jpg', title: 'Swirly Dyes'},
		{src: 'css/images/spyroFire.jpg', title: 'Melted Plastic'},
		{src: 'css/images/jellyFish.jpg', title: 'Jelly Fish'},
		{src: 'css/images/ltFrostedGlass.jpg', title: 'Frosted Glass'},
		{src: 'css/images/pastelBricks.jpg', title: 'Pastel Bricks'},
	],
	bgSelected: {src: 'css/images/fur.jpg', title: 'Color Fur'},
};
defaultData = {
	reqCheckin: true,
	families: [
		{name: 'Larsen', active: true, adult1: "David", adult2: "Kimberly", edit: '', kids: true, members: [ 
			{first: "David", last: "Larsen", dob: "1982-08-29", gender: "M", allergies: 'None', grade: "Adult"},
			{first: "Kimberly", last: "Larsen", dob: "1982-09-23", gender: "F", allergies: 'None', grade: "Adult"},
			{first: "Isaiah", last: "Larsen", dob: "2010-03-22", gender: "M", allergies: 'Dairy, Soy, Peanuts, Fish, Ponies', grade: "Child"},
			{first: "Arianna", last: "Larsen", dob: "2008-11-24", gender: "F", allergies: 'Math, Chores', grade: "Child"},
			{first: "Elizabeth", last: "Larsen", dob: "2015-12-15", gender: "F", allergies: 'None', grade: "Child"} 
		]},
		{name: 'White', active: true, adult1: "Larry", adult2: "Kelly", edit: '', kids: true, members: [ 
			{first: "Larry", last: "White", dob: "1982-08-29", gender: "M", allergies: 'None', grade: "Adult"},
			{first: "Kelley", last: "White", dob: "1982-09-23", gender: "F", allergies: 'None', grade: "Adult"},
			{first: "Josh", last: "White", dob: "2010-03-22", gender: "M", allergies: 'Salad', grade: "Child"},
			{first: "Krystal", last: "White", dob: "2010-03-22", gender: "F", allergies: 'None', grade: "Child"},
			{first: "Andi", last: "White", dob: "2010-03-22", gender: "F", allergies: 'None', grade: "Child"},
		]},
		{name: 'White', active: true, adult1: "Krystal", adult2: "", edit: '', kids: true, members: [
		{first: "Krystal", last: "White", dob: "2010-03-22", gender: "F", allergies: 'None', grade: "Adult"},
			{first: "Gracie", last: "White", dob: "2005-09-08", gender: "F", allergies: 'None', grade: "Child"},
		]},
	],
	famTemp: [],
}