# README #

### Church Check In Label Maker ###
	+ A web app for registering, checking in, printing labels, and tracking attendance for church ministries.
	+ Version: 
	+ Platform: This project was created in HTML/CSS/JS using the Angular 1 Framework.

### How do I get set up? ###

* Summary of set up
	1. Install NODEJS and GIT
	2. Install http-server: npm install -g http-server
	3. Navigate to the folder on your webserver where you want to host the webApp
	4. Execute: git clone https://songoflight@bitbucket.org/songoflight/church-check-in-label-maker.git
	5. Run: cd ./regKids
	6. Open the firewall for port 8080 with: iptables -I INPUT 1 -i eth0 -p tcp --dport 8080 -j ACCEPT
	7. Run: http-server

* Configuration
	+ By default the Admin code is: 0000, type this into the family name search bar to access the Admin menu

* Demo
	+ Here is a sandbox demo you can play with: http://138.197.203.190:8080/index.html
	+ All config/data changes are saved to local storage, and as such will only be persistent per-browser/device

### Who do I talk to? ###
* Repo owner/admin: David Larsen (knewsong@gmail.com)

### Useful Links ###
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)